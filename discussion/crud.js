let http = require("http");

//  Mock database
let directory = [
	{
		"name" : "Rendon",
		"email": "rendonlabarador@gmail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@gmail.com"
	},
	{
		"name" : "CongTV",
		"email" : "congtv@gmail.com"
	},
	{
		"name" : "Pingris",
		"email" : "pingris@gmail.com"
	}
];

http.createServer(function(request, response){

	if(request.url == "/users" && request.method == "GET"){
		response.writeHead(200, {'Content-Type' : 'application/json'});

		// JSON.stringify() - transforms a js into Json string
		response.write(JSON.stringify(directory));
		response.end();
	}

	//  Route for creating a new item upon recieving a POST request
	// A request object contains several parts:
	// Headers = contains information about the request context/content like what is the data type.
	//  Body - contains the actual information being sent with the request.

	// Note : the request body can NOT be empty

	else if(request.url == "/users" && request.method == "POST"){

		// Declare and initialize a "requestBody" variable to an empty string
		// This will act as a placeholder for the resource/data to be created later on.


		let requestBody = '';

		// A stream is sequnce of data
		// Data is receive from the client and is processed in the 'data' stream
		// The information provided from the request object enters a sequence called "data" the code below will be trigerred
		// data step - this reads the "data" stream and processes it as the request body.


		// on method binds an event to an object
		// It is a way to express your intent of there is something happening (data sent or error in your case), execute the function added as a parameter. This style of programming is called Event-driven programming
		request.on('data', function(data){

			requestBody += data
		});

		request.on('end', function(){
			// Check if at this point the requestBody is of data type STRING
			// We need this to be of data type JSON to access its property
 			console.log(typeof requestBody);

 			// JSON.parse() - takes a JSON string and transform it into js objects.

 			requestBody = JSON.parse(requestBody);

 			// Create a new object representing the new  mock database record

 			let newUser = {
 				"name" : requestBody.name,
 				"email" : requestBody.email
 			}

 			// Add the user into mock database
 			directory.push(newUser);
 			console.log(directory);

 			response.writeHead(200, {'Content-Type' : 'application/json'});
 			response.write(JSON.stringify(newUser));
 			response.end();

		});
	}

}).listen(4000);

console.log("Server running at localhost:4000");